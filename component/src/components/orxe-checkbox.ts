import { html, LitElement, customElement, property } from "lit-element";
import { ifDefined } from 'lit-html/directives/if-defined';
import checkboxScss from './orxe-checkbox.scss';
let checkBoxIds = 0;

@customElement("orxe-checkbox")
export default class OrxeCheckbox extends LitElement {

  checkBoxId = `orxe-check-${checkBoxIds++}`;

  /**
 * `true` if the check box should be checked. Corresponds to the attribute with the same name.
 */

  @property({ type: Boolean, reflect: true })
  checked = false;

  /**
   * `true` if the check box should be disabled. 
   */

  @property({ type: Boolean })
  disabled = false;

  /**
   * `true` if the label should be hidden. 
   */

  @property({ type: Boolean, reflect: true, attribute: 'hide-label' })
  hideLabel = false;

  /**
   * `true` if the check box should show its UI of the indeterminate state. 
   */

  @property({ type: Boolean, reflect: true })
  indeterminate = false;

  /**
   * The name of the control, which is submitted with the form data.
   */

  @property({ type: String })
  name = '';

  /**
   * The value. Corresponds to the attribute with the same name.
   */

  @property({ type: String })
  value = '';

  /**
   * If `true`, the checkbox will have right to left feature
   */

  @property({ type: Boolean })
  rtl = true;
  /**
   * Set checkbox label 
   */

  @property({ type: String })
  label = "checkbox";

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
  }

  firstUpdated() {

    if (this.checked) {
      this.indeterminate = false;
    }
  }

  handleChange(event: any) {
    let changeEvent = new Event('orxeOnChange', { bubbles: true, composed: true });
    this.dispatchEvent(changeEvent);
    this.checked = event.target.checked;
    this.indeterminate = false;
  }

  private onFocus() {
    let onFocusEvent = new Event('orxeOnFocus', { bubbles: true, composed: true });
    this.dispatchEvent(onFocusEvent);
  }

  private onBlur() {
    let onBlurEvent = new Event('orxeOnBlur', { bubbles: true, composed: true });
    this.dispatchEvent(onBlurEvent);
  }

  render() {
    let name = ifDefined(this.name == null ? undefined : this.name);
    let value = ifDefined(this.value == null ? undefined : this.value);
    let indeterminate = this.indeterminate ? 'mixed' : String(Boolean(this.checked));
    let  data = document.querySelector("orxe-checkbox");
    console.log(data)
    return html`
        <div class="bx--form-item bx--checkbox-wrapper checkbox-container"  >
          <input id="${this.checkBoxId}"  class="bx--checkbox" type="checkbox" 
            name="${name}"
            value="${value}"
            role="checkbox"
            aria-checked="${indeterminate}"
            aria-disabled=${this.disabled ? 'true' : null}
            ?disabled = ${this.disabled}
            ?checked=${this.checked}
            @change="${this.handleChange}"
            @focus=${this.onFocus}
            @blur=${this.onBlur}>
            <label for="${this.checkBoxId}"  
            class="bx--checkbox-label" 
            data-contained-checkbox-state="${this.indeterminate ? 'mixed' : null}">
              ${this.hideLabel ? '' : this.label}
            </label>
        </div>   `
  }

  createRenderRoot() {
    return this;
  }

  static styles = checkboxScss;
}
